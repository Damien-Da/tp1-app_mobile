package com.example.AMtp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import static com.example.AMtp1.data.Country.countries;

public class DetailFragment extends Fragment {
    TextView itemTitle;
    ImageView itemImage;
    EditText itemCapital;
    EditText itemLanguage;
    EditText itemCurrency;
    EditText itemPopulation;
    EditText itemArea;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemTitle = view.findViewById(R.id.title_second);
        itemImage = view.findViewById(R.id.image_second);
        itemCapital = view.findViewById(R.id.capitalEdit_second);
        itemLanguage = view.findViewById(R.id.languageEdit_second);
        itemCurrency = view.findViewById(R.id.currencyEdit_second);
        itemPopulation = view.findViewById(R.id.populationEdit_second);
        itemArea = view.findViewById(R.id.areaEdit_second);

        int position = DetailFragmentArgs.fromBundle(getArguments()).getCountryId();

        Context c = itemImage.getContext();
        itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(countries[position].getImgUri(), null, c.getPackageName())));
        itemTitle.setText(countries[position].getName());
        itemCapital.setText(countries[position].getCapital());
        itemLanguage.setText(countries[position].getLanguage());
        itemCurrency.setText(countries[position].getCurrency());
        itemPopulation.setText(String.valueOf(countries[position].getPopulation()));
        itemArea.setText(countries[position].getArea()+" km²");

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}